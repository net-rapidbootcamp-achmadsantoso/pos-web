﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly CategoryService _service;
        
        public CategoryController(ApplicationContext context)
        {
            _service = new CategoryService(context);
        }

        public IActionResult Index()
        {
            var Data = _service.GetCategories();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return PartialView("Add");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("CategoryName, Description")] CategoryModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new CategoryEntity(request));
                return Redirect("Index");
            }
            return View("Add", request);

        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _service.View(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var Data = _service.Edit(id);
            return PartialView("Edit", Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("Id, CategoryName, Description")] CategoryModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.Delete(id);
            return Redirect("/Category");
        }

    }
}
