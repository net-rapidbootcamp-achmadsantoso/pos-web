﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class SupplierController : Controller
    {
        private readonly SupplierService _service;
        public SupplierController(ApplicationContext context)
        {
            _service = new SupplierService(context);
        }

        public IActionResult Index()
        {
            var Data = _service.GetAllSupplier();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        public IActionResult AddModal()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, HomePage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new SupplierEntity(request));
                return Redirect("Index");
            }
            return View("_Add", request);

        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _service.GetSupplier(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var Data = _service.Edit(id);
            return View(Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("Id, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, HomePage")] SupplierModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
