﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class OrderController : Controller
    {
        private readonly OrderService _orderService;
        private readonly CustomerService _customerService;
        private readonly EmployeeService _employeeService;
        private readonly ShipperService _shipperService;
        private readonly ProductService _productService;

        public OrderController(ApplicationContext context)
        {
            _orderService = new OrderService(context);
            _customerService = new CustomerService(context);
            _employeeService = new EmployeeService(context);
            _shipperService = new ShipperService(context);
            _productService = new ProductService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var orders = _orderService.GetOrders();
            return View(orders);
        }

        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.Customer = new SelectList(_customerService.GetAllCustomer(), "Id", "Name");
            ViewBag.Employee = new SelectList(_employeeService.GetAllEmployee(), "Id", "LastName");
            ViewBag.Shipper = new SelectList(_shipperService.GetAllShipper(), "Id", "CompanyName");
            ViewBag.Product = new SelectList(_productService.GetProducts(), "Id", "ProductName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save(
            [Bind("CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipperId, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry, OrderDetails")] OrderModel request)
        {
            if (ModelState.IsValid)
            {
                _orderService.CreateOrder(request);
                return Redirect("Index");
            }
            return View("Add", request);
        }

        /*[HttpGet]
        public IActionResult Details(int? id)
        {
            var order = _orderService.ReadOrder(id);
            return View(order);
        }*/
        [HttpGet]
        public IActionResult Details(int? id)
        {
            var order = _orderService.ReadOrderInvoice(id);
            return View(order);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            ViewBag.Customer = new SelectList(_customerService.GetAllCustomer(), "Id", "Name");
            ViewBag.Employee = new SelectList(_employeeService.GetAllEmployee(), "Id", "LastName");
            ViewBag.Shipper = new SelectList(_shipperService.GetAllShipper(), "Id", "CompanyName");
            ViewBag.Product = new SelectList(_productService.GetProducts(), "Id", "ProductName");
            var order = _orderService.ReadOrder(id);

            return View(order);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("Id, CustomerId, EmployeeId, OrderDate, RequiredDate, ShippedDate, ShipperId, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry, OrderDetails")] OrderModel request)
        {
            if (ModelState.IsValid)
            {
                _orderService.UpdateOrder(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _orderService.DeleteCategory(id);
            return Redirect("/Order");
        }
    }
}
