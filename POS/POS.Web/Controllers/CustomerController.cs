﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly CustomerService _service;
        public CustomerController(ApplicationContext context)
        {
            _service = new CustomerService(context);
        }

        public IActionResult Index()
        {
            var Data = _service.GetAllCustomer();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return PartialView("Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("Name, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax")] CustomerModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new CustomerEntity(request));
                return Redirect("Index");
            }
            return View("Add", request);

        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _service.GetCustomer(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var Data = _service.Edit(id);
            return View(Data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("Id, Name, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax")] CustomerModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.Delete(id);
            return RedirectToAction("Index");
        }

    }
}
