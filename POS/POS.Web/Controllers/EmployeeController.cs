﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly EmployeeService _service;
        public EmployeeController(ApplicationContext context)
        {
            _service = new EmployeeService(context);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var data = _service.GetAllEmployee();
            return View(data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return PartialView("Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Notes, ReportTo")]EmployeeModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new EmployeeEntity(request));
                return Redirect("Index");
            }
            return View("Add", request);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var data = _service.GetEmployee(id);
            return View(data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var data = _service.Edit(id);
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("Id, LastName, FirstName, Title, TitleOfCourtesy, BirthDate, HireDate, Address, City, Region, PostalCode, Country, HomePhone, Notes, ReportTo")]EmployeeModel request)
        {
            if (ModelState.IsValid)
            {
                _service.Update(request);
                return Redirect("Index");
            }
            return View("Edit", request);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
