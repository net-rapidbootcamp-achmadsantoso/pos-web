﻿using Microsoft.AspNetCore.Mvc;
using POS.Repository;
using POS.Service;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class ProductController : Controller
    {
        private readonly ProductService _service;
        public ProductController(ApplicationContext context)
        {
            _service = new ProductService(context);
        }
        public IActionResult Index()
        {
            var Data = _service.GetProducts();
            return View(Data);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return PartialView("Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Save([Bind("ProductName, SupplierId, CategoryId, QuantityPerUnit, UnitPrice, UnitInStock, UnitInOrder, ReorderLevel, Discontinued")] ProductModel product)
        {
            if (ModelState.IsValid)
            {
                _service.Add(new ProductEntity(product));
                return Redirect("Index");
            }
            return View("Add", product);
        }

        [HttpGet]
        public IActionResult Details(int? id)
        {
            var Data = _service.View(id);
            return View(Data);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            var data = _service.Edit(id);
            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update([Bind("Id, ProductName, SupplierId, CategoryId, QuantityPerUnit, UnitPrice, UnitInStock, UnitInOrder, ReorderLevel, Discontinued ")] ProductModel product)
        {
            if (ModelState.IsValid)
            {
                _service.Update(product);
                return Redirect("Index");
            }
            return View("Edit", product);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            _service.Delete(id);
            return Redirect("/Product");
        }
    }
}
