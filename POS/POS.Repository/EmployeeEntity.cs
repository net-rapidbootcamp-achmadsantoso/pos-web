﻿using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repository
{
    [Table("tbl_employees")]
    public class EmployeeEntity
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("last_name")]
        public string LastName { get; set; }
        [Column("first_name")]
        public string FirstName { get; set; }
        [Column("title")]
        public string Title { get; set; }
        [Column("title_of_courtesy")]
        public string TitleOfCourtesy { get; set; }
        [Column("birth_date")]
        public DateTime BirthDate { get; set; }
        [Column("hire_date")]
        public DateTime HireDate { get; set; }
        [Column("address")]
        public string Address { get; set; }
        [Column("city")]
        public string City { get; set; }
        [Column("region")]
        public string Region { get; set; }
        [Column("postal_code")]
        public string PostalCode { get; set; }
        [Column("country")]
        public string Country { get; set; }
        [Column("home_phone")]
        public string HomePhone { get; set; }
        [Column("notes")]
        public string Notes { get; set; }
        [Column("report_to")]
        public int ReportTo { get; set; }
        
        public ICollection<OrderEntity> orderEntities { get; set; }

        public EmployeeEntity(EmployeeModel model)
        {
            LastName = model.LastName; 
            FirstName = model.FirstName;
            Title = model.Title;
            TitleOfCourtesy = model.TitleOfCourtesy;
            BirthDate = model.BirthDate;
            HireDate = model.HireDate;
            Address = model.Address;
            City = model.City;
            Region = model.Region;
            PostalCode = model.PostalCode;
            Country = model.Country;
            HomePhone = model.HomePhone;
            Notes = model.Notes;
            ReportTo = model.ReportTo;
        }

        public EmployeeEntity()
        {
            
        }
    }
}
