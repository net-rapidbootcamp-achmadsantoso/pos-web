﻿using Microsoft.EntityFrameworkCore;

namespace POS.Repository
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) 
        { 
        }
        
        public DbSet<ProductEntity> productEntities => Set<ProductEntity>();
        public DbSet<CategoryEntity> categoryEntities => Set<CategoryEntity>();
        public DbSet<SupplierEntity> supplierEntities => Set<SupplierEntity>();
        public DbSet<CustomerEntity> customerEntities => Set<CustomerEntity>();
        public DbSet<EmployeeEntity> employeeEntities => Set<EmployeeEntity>();
        public DbSet<OrderDetailEntity> orderDetailEntities => Set<OrderDetailEntity>();
        public DbSet<OrderEntity> orderEntities => Set<OrderEntity>();
        public DbSet<ShipperEntity> shipperEntities => Set<ShipperEntity>();
    }
}
