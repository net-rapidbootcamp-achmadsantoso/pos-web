﻿using POS.Repository;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class ProductService
    {
        private readonly ApplicationContext _context;
        private ProductModel EntityToModel(ProductEntity entity)
        {
            ProductModel model = new ProductModel();
            model.Id = entity.Id;
            model.ProductName = entity.ProductName;
            model.SupplierId= entity.SupplierId;
            model.CategoryId= entity.CategoryId;
            model.QuantityPerUnit= entity.QuantityPerUnit;
            model.UnitPrice = entity.UnitPrice;
            model.UnitInStock = entity.UnitInStock;
            model.UnitInOrder = entity.UnitInOrder;
            model.ReorderLevel = entity.ReorderLevel;
            model.Discontinued = entity.Discontinued;
            return model;
        }

        private void ModelToEntity(ProductModel model, ProductEntity entity)
        {
            entity.ProductName = model.ProductName;
            entity.SupplierId = model.SupplierId;
            entity.CategoryId = model.CategoryId;
            entity.QuantityPerUnit = model.QuantityPerUnit;
            entity.UnitPrice = model.UnitPrice;
            entity.UnitInStock = model.UnitInStock;
            entity.UnitInOrder = model.UnitInOrder;
            entity.ReorderLevel = model.ReorderLevel;
            entity.Discontinued = model.Discontinued;
        }
        public ProductService(ApplicationContext context)
        {
            _context= context;
        }

        public List<ProductEntity> GetProducts()
        {
            return _context.productEntities.ToList();
        }

        public void Add(ProductEntity products)
        {
            _context.productEntities.Add(products);
            _context.SaveChanges();
        }

        public ProductModel View(int? id)
        {
            var product = _context.productEntities.Find(id);
            return EntityToModel(product);
        }

        public ProductModel Edit(int? id)
        {
            var product = _context.productEntities.Find(id);
            return EntityToModel(product);
        }

        public void Update(ProductModel model)
        {
            var product = _context.productEntities.Find(model.Id);
            ModelToEntity(model, product);
            _context.productEntities.Update(product);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var data = _context.productEntities.Find(id);
            _context.productEntities.Remove(data);
            _context.SaveChanges();
        }
    }
}
