﻿using POS.Repository;
using POS.ViewModel;
using POS.ViewModel.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Service
{
    public class OrderService
    {
        private readonly ApplicationContext _context;

        public OrderService(ApplicationContext context)
        {
            _context = context;
        }

        private OrderModel EntityToModelOrder(OrderEntity entity)
        {
            var model = new OrderModel();
            model.Id = entity.Id;
            model.CustomerId = entity.CustomerId;
            model.EmployeeId = entity.EmployeeId;
            model.OrderDate = entity.OrderDate;
            model.RequiredDate = entity.RequiredDate;
            model.ShippedDate = entity.ShippedDate;
            model.ShipperId = entity.ShipperId;
            model.Freight = entity.Freight;
            model.ShipName = entity.ShipName;
            model.ShipAddress = entity.ShipAddress;
            model.ShipCity = entity.ShipCity;
            model.ShipRegion = entity.ShipRegion;
            model.ShipPostalCode = entity.ShipPostalCode;
            model.ShipCountry = entity.ShipCountry;
            model.OrderDetails = new List<OrderDetailModel>();
            foreach (var item in entity.OrderDetails)
            {
                model.OrderDetails.Add(EntityToModelOrderDetail(item));
            }

            return model;
        }

        private OrderEntity ModelToEntityOrder(OrderModel model)
        {
            var entity = new OrderEntity();
            entity.CustomerId = model.CustomerId;
            entity.EmployeeId = model.EmployeeId;
            entity.OrderDate = model.OrderDate;
            entity.RequiredDate = model.RequiredDate;
            entity.ShippedDate = model.ShippedDate;
            entity.ShipperId = model.ShipperId;
            entity.Freight = model.Freight;
            entity.ShipName = model.ShipName;
            entity.ShipAddress = model.ShipAddress;
            entity.ShipCity = model.ShipCity;
            entity.ShipRegion = model.ShipRegion;
            entity.ShipPostalCode = model.ShipPostalCode;
            entity.ShipCountry = model.ShipCountry;
            entity.OrderDetails = new List<OrderDetailEntity>();

            foreach (var item in model.OrderDetails)
            {
                entity.OrderDetails.Add(ModelToEntityOrderDetail(item));
            }

            return entity;
        }

        private OrderDetailModel EntityToModelOrderDetail(OrderDetailEntity entity)
        {
            var model = new OrderDetailModel();
            model.Id = entity.Id;
            model.ProductId = entity.ProductId;
            model.UnitPrice = entity.UnitPrice;
            model.Quantity = entity.Quantity;
            model.Discount = entity.Discount;

            return model;
        }

        private OrderDetailEntity ModelToEntityOrderDetail(OrderDetailModel model)
        {
            var entity = new OrderDetailEntity();
            entity.OrderId = model.OrderId;
            entity.ProductId = model.ProductId;
            entity.UnitPrice = model.UnitPrice;
            entity.Quantity = model.Quantity;
            entity.Discount = model.Discount;

            return entity;
        }

        private OrderResponse EntityToModelResponseDetail(OrderEntity entity)
        {
            var customer = _context.customerEntities.Find(entity.CustomerId);
            var shipper = _context.shipperEntities.Find(entity.ShipperId);

            var response = new OrderResponse();
            response.Id = entity.Id;
            response.CustomerId = customer.Id;
            response.CustomerName = customer.Name;
            response.OrderDate = entity.OrderDate;
            response.RequiredDate = entity.RequiredDate;
            response.ShippedDate = entity.ShippedDate;
            response.ShipperId = shipper.Id;
            response.ShipperName = shipper.CompanyName;
            response.ShipperPhone = shipper.Phone;
            response.Freight = entity.Freight;
            response.ShipName = entity.ShipName;
            response.ShipAddress = entity.ShipAddress;
            response.ShipCity = entity.ShipCity;
            response.ShipRegion = entity.ShipRegion;
            response.ShipPostalCode = entity.ShipPostalCode;
            response.ShipCountry = entity.ShipCountry;
            response.Details = new List<OrderDetailResponse>();

            foreach (var item in entity.OrderDetails)
            {
                response.Details.Add(EntityToModelDetailResponse(item));
            }
            var subtotal = 0.0;
            foreach (var item in response.Details)
            {
                item.Subtotal = item.Quantity * item.UnitPrice * (1 - item.Discount / 100);
                subtotal += item.Subtotal;
            }
            response.Subtotal = subtotal;
            response.Tax = 0.1 * subtotal;
            response.Shipping = 0;
            response.Total = response.Subtotal + response.Tax + response.Shipping;

            return response;
        }

        private OrderDetailResponse EntityToModelDetailResponse(OrderDetailEntity entity)
        {
            var model = new OrderDetailResponse();
            var product = _context.productEntities.Find(entity.ProductId);

            model.Id = entity.Id;
            model.ProductId = product.Id;
            model.ProductName = product.ProductName;
            model.UnitPrice = entity.UnitPrice;
            model.Quantity = entity.Quantity;
            model.Discount = entity.Discount;

            return model;
        }

        public List<OrderEntity> GetOrders()
        {
            return _context.orderEntities.ToList();
        }

        public void CreateOrder(OrderModel newOrder)
        {
            var newData = ModelToEntityOrder(newOrder);
            _context.orderEntities.Add(newData);
            foreach (var item in newData.OrderDetails)
            {
                item.OrderId = newOrder.Id;
                _context.orderDetailEntities.Add(item);
            }
            _context.SaveChanges();
        }

        public OrderModel ReadOrder(int? id)
        {
            var order = _context.orderEntities.Find(id);
            var detail = _context.orderDetailEntities.Where(x => x.OrderId == id);
            foreach (var item in detail) { }
            return EntityToModelOrder(order);
        }

        public OrderResponse ReadOrderInvoice(int? id)
        {
            var orderEntity = _context.orderEntities.Find(id);
            var detailEntity = _context.orderDetailEntities.Where(x => x.OrderId == id).ToList();
            orderEntity.OrderDetails = detailEntity;
            var orderResponse = EntityToModelResponseDetail(orderEntity);
            return orderResponse;
        }

        public void UpdateOrder(OrderModel updatedOrder)
        {
            // read from database
            var entityOrder = _context.orderEntities.Find(updatedOrder.Id);
            var orderDetailList = _context.orderDetailEntities.Where(x => x.OrderId == updatedOrder.Id).ToList();

            var updatedEntity = ModelToEntityOrder(updatedOrder);

            entityOrder.CustomerId = updatedEntity.CustomerId;
            entityOrder.EmployeeId = updatedEntity.EmployeeId;
            entityOrder.OrderDate = updatedEntity.OrderDate;
            entityOrder.RequiredDate = updatedEntity.RequiredDate;
            entityOrder.ShippedDate = updatedEntity.ShippedDate;
            entityOrder.ShipperId = updatedEntity.ShipperId;
            entityOrder.Freight = updatedEntity.Freight;
            entityOrder.ShipName = updatedEntity.ShipName;
            entityOrder.ShipAddress = updatedEntity.ShipAddress;
            entityOrder.ShipCity = updatedEntity.ShipCity;
            entityOrder.ShipRegion = updatedEntity.ShipRegion;
            entityOrder.ShipPostalCode = updatedEntity.ShipPostalCode;
            entityOrder.ShipCountry = updatedEntity.ShipCountry;
            entityOrder.OrderDetails = updatedEntity.OrderDetails;

            /*request.OrderDetails.Clear();
            request.OrderDetails = updatedOrder.OrderDetails;*/

            _context.orderEntities.Update(entityOrder);
            foreach (var newItem in entityOrder.OrderDetails)
            {
                newItem.OrderId = updatedOrder.Id;
                foreach (var item in orderDetailList)
                {
                    if (newItem.ProductId == item.ProductId)
                    {
                        item.ProductId = newItem.ProductId;
                        item.UnitPrice = newItem.UnitPrice;
                        item.Quantity = newItem.Quantity;
                        item.Discount = newItem.Discount;
                        _context.orderDetailEntities.Update(item);
                    }
                }

            }
            _context.SaveChanges();
        }

        public void DeleteCategory(int? id)
        {
            var order = _context.orderEntities.Find(id);
            _context.orderEntities.Remove(order);

            var detail = _context.orderDetailEntities.Where(_x => _x.Id == id);
            foreach (var item in detail)
            {
                _context.orderDetailEntities.Remove(item);
            }

            _context.SaveChanges();
        }
    }
}
