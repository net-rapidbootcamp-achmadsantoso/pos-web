﻿using POS.Repository;
using POS.ViewModel;
using System.Runtime.InteropServices;

namespace POS.Service
{
    public class CustomerService
    {
        private readonly ApplicationContext _context;

        private CustomerModel EntityToModel(CustomerEntity enity)
        {
            CustomerModel model = new CustomerModel();
            model.Id = enity.Id;
            model.Name = enity.Name;
            model.ContactName = enity.ContactName;
            model.Address = enity.Address;
            model.City = enity.City;
            model.Region = enity.Region;
            model.PostalCode = enity.PostalCode;
            model.Country = enity.Country;
            model.Phone = enity.Phone;
            model.Fax = enity.Fax;
            model.ContactTitle = enity.ContactTitle;
            return model;
        } 

        private void ModelToEntity(CustomerModel model, CustomerEntity entity)
        {
            entity.Name = model.Name;
            entity.ContactName = model.ContactName;
            entity.ContactTitle = model.ContactTitle;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.Phone = model.Phone;
            entity.Fax = model.Fax;
        }
        public CustomerService(ApplicationContext context)
        {
            _context = context;
        }

        public List<CustomerEntity> GetAllCustomer()
        {
            return _context.customerEntities.ToList();
        }

        public void Add(CustomerEntity customers)
        {
            _context.customerEntities.Add(customers);
            _context.SaveChanges();
        }

        public CustomerModel GetCustomer(int? id)
        {
            var customer = _context.customerEntities.Find(id);
            return EntityToModel(customer);
        }

        public CustomerModel Edit(int? id)
        {
            var customer = _context.customerEntities.Find(id);
            return EntityToModel(customer);
        }

        public void Update(CustomerModel customer)
        {
            var getId = _context.customerEntities.Find(customer.Id);
            ModelToEntity(customer, getId);
            _context.customerEntities.Update(getId);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var data = _context.customerEntities.Find(id);
            _context.customerEntities.Remove(data);
            _context.SaveChanges();
        }

    }
}