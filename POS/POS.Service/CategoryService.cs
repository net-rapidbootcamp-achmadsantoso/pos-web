﻿using POS.Repository;
using POS.ViewModel;
using System.Runtime.InteropServices;

namespace POS.Service
{
    public class CategoryService
    {
        private readonly ApplicationContext _context;
        private CategoryModel EntityToModel (CategoryEntity entity)
        {
            CategoryModel result = new CategoryModel();
            result.Id = entity.Id;
            result.CategoryName = entity.CategoryName;
            result.Description = entity.Description;
            return result;
        }

        private void ModelToEntity (CategoryModel model, CategoryEntity entity)
        {
            entity.CategoryName = model.CategoryName;
            entity.Description = model.Description;
        }
        public CategoryService(ApplicationContext context)
        {
            _context = context;
        }

        public List<CategoryEntity> GetCategories()
        {
            return _context.categoryEntities.ToList();
        }

        public void Add(CategoryEntity category)
        {
            _context.categoryEntities.Add(category);
            _context.SaveChanges();
        }

        public CategoryModel View(int? id)
        {
            var category = _context.categoryEntities.Find(id);
            return EntityToModel(category);
        }

        public CategoryModel Edit(int? id)
        {
            var category = _context.categoryEntities.Find(id);
            return EntityToModel(category);
        }

        public void Update(CategoryModel category)
        {
            var getById = _context.categoryEntities.Find(category.Id);
            ModelToEntity(category, getById);
            _context.categoryEntities.Update(getById);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var data = _context.categoryEntities.Find(id);
            _context.categoryEntities.Remove(data);
            _context.SaveChanges();
        }

    }
}