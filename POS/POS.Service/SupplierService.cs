﻿using POS.Repository;
using POS.ViewModel;
using System.Runtime.InteropServices;

namespace POS.Service
{
    public class SupplierService
    {
        private readonly ApplicationContext _context;
        private SupplierModel EntityToModel(SupplierEntity suppliers)
        {
            SupplierModel model = new SupplierModel();
            model.Id = suppliers.Id;
            model.CompanyName = suppliers.CompanyName;
            model.ContactName = suppliers.ContactName;
            model.ContactTitle = suppliers.ContactTitle;
            model.Address = suppliers.Address;
            model.City = suppliers.City;
            model.Region = suppliers.Region;
            model.PostalCode = suppliers.PostalCode;
            model.Country = suppliers.Country;
            model.Phone = suppliers.Phone;
            model.Fax = suppliers.Fax;
            model.HomePage = suppliers.HomePage;
            return model;
        }

        private void ModelToEntity(SupplierModel model, SupplierEntity suppliers)
        {
            suppliers.CompanyName = model.CompanyName;
            suppliers.ContactName = model.ContactName;
            suppliers.ContactTitle = model.ContactTitle;
            suppliers.Phone = model.Phone;
            suppliers.Fax = model.Fax;
            suppliers.Address = model.Address;
            suppliers.City = model.City;
            suppliers.Region = model.Region;
            suppliers.PostalCode = model.PostalCode;
            suppliers.Country = model.Country;
            suppliers.HomePage = model.HomePage;
        }

        public SupplierService(ApplicationContext context)
        {
            _context = context;
        }

        public List<SupplierEntity> GetAllSupplier()
        {
            return _context.supplierEntities.ToList();
        }

        public void Add(SupplierEntity suppliers)
        {
            _context.supplierEntities.Add(suppliers);
            _context.SaveChanges();
        }

        public SupplierModel GetSupplier(int? id)
        {
            var supplier = _context.supplierEntities.Find(id);
            return EntityToModel(supplier);
        }

        public SupplierModel Edit(int? id)
        {
            var supplier = _context.supplierEntities.Find(id);
            return EntityToModel(supplier);
        }

        public void Update(SupplierModel model)
        {
            var supplier = _context.supplierEntities.Find(model.Id);
            ModelToEntity(model, supplier);
            _context.supplierEntities.Update(supplier);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var data = _context.supplierEntities.Find(id);
            _context.supplierEntities.Remove(data);
            _context.SaveChanges();
        }

    }
}