﻿using POS.Repository;
using POS.ViewModel;
using System.Runtime.InteropServices;

namespace POS.Service
{
    public class EmployeeService
    {
        private readonly ApplicationContext _context;
        private EmployeeModel EntityToModel (EmployeeEntity entity)
        {
            EmployeeModel result = new EmployeeModel();
            result.Id = entity.Id;
            result.LastName = entity.LastName;
            result.FirstName = entity.FirstName;
            result.BirthDate = entity.BirthDate;
            result.HireDate = entity.HireDate;
            result.Title = entity.Title;
            result.TitleOfCourtesy = entity.TitleOfCourtesy;
            result.Address = entity.Address;
            result.City = entity.City;
            result.Region = entity.Region;
            result.PostalCode = entity.PostalCode;
            result.Country = entity.Country;
            result.HomePhone = entity.HomePhone;
            result.Notes = entity.Notes;
            result.ReportTo = entity.ReportTo;

            return result;
        }

        private void ModelToEntity (EmployeeModel model, EmployeeEntity entity)
        {
            entity.LastName = model.LastName;
            entity.FirstName = model.FirstName;
            entity.Title = model.Title;
            entity.TitleOfCourtesy = model.TitleOfCourtesy;
            entity.BirthDate = model.BirthDate;
            entity.HireDate = model.HireDate;
            entity.Address = model.Address;
            entity.City = model.City;
            entity.Region = model.Region;
            entity.PostalCode = model.PostalCode;
            entity.Country = model.Country;
            entity.HomePhone = model.HomePhone;
            entity.Notes = model.Notes;
            entity.ReportTo = model.ReportTo;
        }
        public EmployeeService(ApplicationContext context)
        {
            _context = context;
        }

        public List<EmployeeEntity> GetAllEmployee()
        {
            return _context.employeeEntities.ToList();
        }

        public void Add(EmployeeEntity employee)
        {
            _context.employeeEntities.Add(employee);
            _context.SaveChanges();
        }

        public EmployeeModel GetEmployee(int? id)
        {
            var employee = _context.employeeEntities.Find(id);
            return EntityToModel(employee);
        }

        public EmployeeModel Edit(int? id)
        {
            var employee = _context.employeeEntities.Find(id);
            return EntityToModel(employee);
        }

        public void Update(EmployeeModel employee)
        {
            var getById = _context.employeeEntities.Find(employee.Id);
            ModelToEntity(employee, getById);
            _context.employeeEntities.Update(getById);
            _context.SaveChanges();
        }

        public void Delete(int? id)
        {
            var data = _context.employeeEntities.Find(id);
            _context.employeeEntities.Remove(data);
            _context.SaveChanges();
        }

    }
}